declare global {

  export type ArraySelector =
    | `${number}:${number}:${number}`
    | `:${number}:${number}`
    | `::${number}`
    | `${number}::${number}`
    | `${number}:${number}`
    | `:${number}`
    | `${number}:`
    | ':'

  export interface Array<T> {
    /**
     * Sums up all values in the array
     * @return {number} The sum of the values or 0 for an empty array
     */
    sum(this: number[]): number,

    /**
     * Multiplies up all values in the array
     * @return {number} The product of the values or 1 for an empty array
     */
    prod(this: number[]): number,

    /**
     * Finds the maximum value in the array
     * @return {number} The maximum value for an empty array
     * @throws On empty array
     */
    max(this: number[]): number,

    /**
     * Finds the maximum value in the array
     * @param {(a: T, b: T) => number} comparatorFn The comparator function to use
     * @return {number} The maximum value for an empty array
     * @throws error on empty array
     */
    max(comparatorFn: (a: T, b: T) => number): T,

    /**
     * Finds the minimum value in the array
     * @return {number} The minimum value for an empty array
     * @throws On empty array
     */
    min(this: number[]): number,

    /**
     * Finds the minimum value in the array
     * @param {(a: T, b: T) => number} comparatorFn The comparator function to use
     * @return {number} The minimum value or for an empty array
     * @throws On empty array
     */
    min(comparatorFn: (a: T, b: T) => number): number,

    /**
     * Buffers the array into smaller groups. The last group might not be of equal size.
     * @param {number} count The size of each group
     * @param {number} startEvery Start a new buffer every x elements
     * @return {T[][]} The different groups
     */
    buffer(count: number, startEvery?: number): T[][],

    /**
     * Splits the array depending on a predicate, similar to .split on strings.
     * @param {boolean} predicate The predicate function to determine where to split
     * @param {boolean} keepSplitEntries Determines whether the entries that satisfied the predicate are present in the result (in the new groups) or not.
     * @return {T[][]} The different groups
     */
    split(predicate: (entry: T) => boolean, keepSplitEntries?: boolean): T[][]

    /**
     * Intersects the array with the given other array and keeps only these values that appear in the other array. Values are kept in the minimum quantity of both arrays.
     * @param {S[]} other The other array to intersect with
     * @param {(a: T, b: S) => boolean} equalsFn The function to determine equality
     * @return {T[]} The values that appear in both arrays, in the respective minimum quantities
     */
    intersect<S extends number | string | boolean>(this: S[], other: S[], equalsFn?: (a: T, b: S) => boolean): T[]

    /**
     * Intersects the array with the given other array and keeps only these values that appear in the other array. Values are kept in the minimum quantity of both arrays.
     * @param {S[]} other The other array to intersect with
     * @param {(a: T, b: S) => boolean} equalsFn The function to determine equality
     * @return {T[]} The values that appear in both arrays, in the respective minimum quantities
     */
    intersect<S>(other: S[], equalsFn: (a: T, b: S) => boolean): T[]

    /**
     * Remove element at index, negative index is counted from behind
     * @param {number} index The index to remove
     * @throws On index out of scope
     * @return {T[]} The array with removed element
     */
    remove(index: number): T[]

    /**
     * Remove first element that satisfies predicate
     * @param {(a: T) => boolean} predicate Predicate to check
     * @return {T[]} The array with potentially removed element
     */
    removeFirst(predicate: (a: T) => boolean): T[]

    /**
     * Removes duplicate elements
     * @param {(a: T, b: T) => boolean} equalFn Function to determine equality
     * @return {T[]} The array with unique entries
     */
    unique(equalFn?: (a: T, b: T) => boolean): T[]

    /**
     * Get first element
     * @throws On empty array
     * @return {T} The first element
     */
    first(): T

    /**
     * Get last element
     * @throws On empty array
     * @return {T} The last element
     */
    last(): T

    /**
     * Counts the elements that satisfy the given predicate.
     * @param {(a: T) => boolean} predicate The predicate to check
     * @return {number} The number of elements that satisfy the predicate
     */
    count(predicate: (a: T) => boolean): number

    /**
     * Transposes the matrix
     * @param {boolean} fillWithUndefined Decide whether shorter rows should be filled up with undefined. Default is true.
     * @return {S[][]} The transposed matrix
     */
    transpose<S>(this: S[][], fillWithUndefined?: boolean): S[][]

    /**
     * Get partial array, specified by selector (similar to python selectors)
     * @param {ArraySelector} selector The selector to specify the array part and step size to get
     * @return {T[]} The partial array
     */
    get(this: T[], selector: ArraySelector): T[]

    /**
     * Like reduce but return array with all intermediate results
     * @param {(akkumulator: S, value: T, index?: number) => S} reducer The reducer function
     * @param {S} seed The starting seed
     * @return {S[]} The array with all intermediate values
     */
    scan<S>(this: T[], reducer: (akkumulator: S, value: T, index?: number) => S, seed: S): S[]

    /**
     * Like reduce but return array with all intermediate results
     * @param {(akkumulator: T, value: T, index?: number) => T} reducer The reducer function
     * @return {T[]} The array with all intermediate values
     */
    scan(this: T[], reducer: (akkumulator: T, value: T, index?: number) => T): T[]

    /**
     * Returns a new array that begins with 'value' and then appends the current array
     * @param {T} value The new starting value
     * @return {T[]} The new array
     */
    startWith(this: T[], value: T): T[]

    /**
     * Invokes the given function for each element and returns the unchanged array
     * @return {T[]} The unchanged array
     */
    sideEffect(this: T[], fn: (t: T) => any): T[]

    /**
     * Returns a sorted array. If the array is a number array, use number sorting, otherwise use the default sort
     * @param {(a: T, b: T) => number} comparatorFn The comparator function to use
     * @return {T[]} The sorted array
     */
    sort(this: T[], comparatorFn?: (a: T, b: T) => number): T[]

    /**
     * Returns a sorted array. Map each entry via the given function to a number  and use number sorting for the array
     * @param {(a: T) => number} mapperFn The comparator function to use
     * @return {T[]} The sorted array
     */
    sort(this: T[], mapperFn: (a: T) => number): T[]

    /**
     * Splits the array in groups each time the array value changes. So each part consists of only the same elements
     * @param {(a: T, b: T) => boolean} equalsFn A custom equals function to use instead of ===
     * @return {T[][]} The split array
     */
    groupSameElements(this: T[], equalsFn?: (a: T, b: T) => boolean): T[][]

    /**
     * Returns an array of arrays containing all possible permutations of the source array
     * @return {T[][]} The array of all permutations
     */
    getAllPermutations(this: T[]): T[][]

    /**
     * Partitions the array into two arrays: The first contains all elements that satisfy the predicate, the second one the rest
     * @param {(t: T) => boolean} predicate The predicate to check the elements agains
     * @return {[satisfies: T[], doesNotSatisfy: T[]]} The partitions
     */
    partition(this: T[], predicate: (t: T) => boolean): [satisfies: T[], doesNotSatisfy: T[]]
  }
}

Array.prototype.sum = function (this: number[]): number {
  return this.reduce((a, b) => a + b, 0)
}

Array.prototype.prod = function (this: number[]): number {
  return this.reduce((a, b) => a * b, 1)
}

Array.prototype.max = function <T>(this: T[], comparatorFn: (a: T, b: T) => number = (a, b) => (a as number) - (b as number)): T {
  if (!this.length) {
    throw 'Cannot identify the maximum of an empty array'
  }
  return this.reduce((a, b) => comparatorFn(a, b) >= 0 ? a : b)
}

Array.prototype.min = function <T>(this: T[], comparatorFn: (a: T, b: T) => number = (a, b) => (a as number) - (b as number)): T {
  if (!this.length) {
    throw 'Cannot identify the minimum of an empty array'
  }
  return this.reduce((a, b) => comparatorFn(a, b) <= 0 ? a : b)
}

Array.prototype.buffer = function <T>(this: T[], count: number, startEvery: number = count): T[][] {
  if (!this.length || count <= 0) {
    return []
  }
  return this.reduce(({bufferedArray, startCounter, lastUnfilledPart}, current) => {
    if (startCounter % startEvery === 0) {
      bufferedArray.push([])
    }
    startCounter++

    for (let i = lastUnfilledPart; i < bufferedArray.length; i++) {
      const buffer = bufferedArray[i]
      buffer.push(current)
      if (buffer.length === count) {
        lastUnfilledPart++
      }
    }
    return {
      bufferedArray,
      startCounter,
      lastUnfilledPart,
    }
  }, {bufferedArray: [] as T[][], startCounter: 0, lastUnfilledPart: 0}).bufferedArray
}

Array.prototype.split = function <T>(this: T[], predicate: (entry: T) => boolean, keepSplitEntries = false): T[][] {
  if (!this.length) {
    return []
  }
  return this.reduce((groups, current) => {
    if (predicate(current)) {
      if (keepSplitEntries) {
        groups.push([current])
      } else {
        groups.push([])
      }
    } else {
      groups[groups.length - 1].push(current)
    }
    return groups
  }, [[]] as T[][])
}

Array.prototype.intersect = function <T, S>(this: T[], other: S[], equalsFn: (a: T, b: S) => boolean = (a, b) => (a as any) === (b as any)): T[] {
  if (!other.length || !this.length) {
    return []
  }

  return this.reduce(([result, remaining], current) => {
    const indexInOther = remaining.findIndex(otherEntry => equalsFn(current, otherEntry))
    if (indexInOther === -1) {
      return [result, remaining] as [T[], S[]]
    }
    return [[...result, current], [...remaining.slice(0, indexInOther), ...remaining.slice(indexInOther + 1)]] as [T[], S[]]
  }, [[], other] as [T[], S[]])[0]
}

Array.prototype.remove = function <T>(this: T[], index: number): T[] {
  let indexToRemove: number

  if (index >= this.length || index < -this.length) {
    throw `Index out of bounds. Tried to remove element with index ${index} but array has only ${this.length} elements.`
  }
  indexToRemove = index >= 0 ? index : convertNegativeIndex(index, this.length)

  return [...this.slice(0, indexToRemove), ...this.slice(indexToRemove + 1)]
}

Array.prototype.removeFirst = function <T>(this: T[], predicate: (a: T) => boolean): T[] {
  const indexToRemove = this.findIndex(predicate)
  if (indexToRemove === -1) {
    return this
  }
  return this.remove(indexToRemove)
}

Array.prototype.unique = function <T>(this: T[], equalFn: (a: T, b: T) => boolean = (a, b) => a === b): T[] {
  return this.filter((value, index) => this.findIndex(a => equalFn(value, a)) === index)
}

Array.prototype.first = function <T>(this: T[]): T {
  if (!this.length) {
    throw 'Cannot access first element, array is empty.'
  }
  return this[0]
}

Array.prototype.last = function <T>(this: T[]): T {
  if (!this.length) {
    throw 'Cannot access last element, array is empty.'
  }
  return this[this.length - 1]
}

Array.prototype.count = function <T>(this: T[], predicate: (a: T) => boolean): number {
  return this.filter(predicate).length
}

Array.prototype.transpose = function <S>(this: S[][], fillWithUndefined: boolean = true): S[][] {
  if (!this.length) {
    return []
  }

  const width = this.map(row => row.length).max()

  const result = [] as S[][]
  this.forEach(currentRow => {
    for (let colIndex = 0; colIndex < width; colIndex++) {
      if (!result[colIndex]) {
        result[colIndex] = []
      }
      if (currentRow[colIndex] !== undefined || fillWithUndefined) {
        result[colIndex].push(currentRow[colIndex])
      }
    }
  })

  return result
}


Array.prototype.get = function <T>(this: T[], selector: ArraySelector): T[] {
  const indices = parseSelector(selector, this.length)

  const result = [] as T[]

  for (let i = Math.max(indices.startIndex, 0); i < Math.min(indices.endIndex, this.length); i += indices.stepSize) {
    result.push(this[i])
  }

  return result
}

Array.prototype.scan = function <T, S>(this: T[], ...params: [(akkumulator: S, value: T, index?: number) => S] | [(akkumulator: S, value: T, index?: number) => S, S]): S[] {
  if (!this.length) {
    return []
  }
  return this.get(`${params.length === 2 ? 0 : 1}:`).reduce((akk, val, index) => [...akk, params[0](akk.length ? akk.last() : params[1]!, val, index)], params.length === 2 ? [] : [this.first() as unknown as S])
}

Array.prototype.startWith = function <T>(this: T[], value: T): T[] {
  return [value, ...this]
}

const originalSort = Array.prototype.sort as <T>(compareFn?: (a: T, b: T) => number) => T[]

Array.prototype.sort = function <T>(this: T[], comparatorOrMapperFn?: ((a: T, b: T) => number) | ((a: T) => number)): T[] {
  if (comparatorOrMapperFn) {
    if (comparatorOrMapperFn.length === 1) {
      return this.sort((a, b) => comparatorOrMapperFn(a, null as T) - comparatorOrMapperFn(b, null as T))
    }
    return originalSort.bind(this)(comparatorOrMapperFn as any)
  }
  if (this.every(element => typeof element === 'number')) {
    return originalSort.bind(this)((a, b) => (a as number) - (b as number))
  }
  return originalSort.bind(this)()
}

Array.prototype.sideEffect = function <T>(this: T[], fn: (t: T) => any): T[] {
  this.forEach(v => fn(v))
  return this
}

Array.prototype.groupSameElements = function <T>(this: T[], equalsFn?: (a: T, b: T) => boolean): T[][] {
  if (this.length === 0) {
    return []
  }
  if (this.length === 1) {
    return [[this[0]]]
  }


  return this.buffer(2, 1).get(':-1').reduce((res, [prev, curr]) => {
    if (equalsFn ? equalsFn(prev, curr) : prev === curr) {
      res.last().push(curr)
    } else {
      res.push([curr])
    }
    return res
  }, [[this[0]]] as T[][])
}

Array.prototype.getAllPermutations = function<T>(this: T[]): T[][] {
  if (this.length === 0) {
    return []
  }
  if (this.length === 1) {
    return [this]
  }
  const firstElement = this[0]
  const subPermutations = this.slice(1).getAllPermutations()
  return subPermutations.flatMap(permutation => {
    const newPermutations: T[][] = []
    for (let i = 0; i <= permutation.length; i++) {
      const p = permutation.slice()
      p.splice(i, 0, firstElement)
      newPermutations.push(p)
    }
    return newPermutations
  })
}

Array.prototype.partition = function<T>(this: T[], predicate: (t: T) => boolean): [satisfies: T[], doesNotSatisfy: T[]] {
  const satisfies: T[] = []
  const doesNotSatisfy: T[] = []
  this.forEach(x => predicate(x) ? satisfies.push(x) : doesNotSatisfy.push(x))
  return [satisfies, doesNotSatisfy]
}

function convertNegativeIndex(index: number, arrayLength: number, allowOutOfBounds: boolean = false): number {
  if (index >= 0) {
    throw `Index ${index} is not negative.`
  }
  if ((-index > arrayLength) && !allowOutOfBounds) {
    throw `Negative index too low. Minimum index is -${arrayLength} but tried to access ${index}.`
  }
  return arrayLength + index
}

const SELECTOR_REGEX = /^((-?\d+)?:)?((-?\d+)?:?)(-?\d+)?$/
// a:b:c
// 2 = a
// 4 = b
// 5 = c

function parseSelector(selector: ArraySelector, arrayLength: number) {
  const regexResult = SELECTOR_REGEX.exec(selector)
  if (!regexResult) {
    throw `Could not parse selector "${selector}".`
  }

  const startIndex = Number(regexResult[2] ?? 0)
  const endIndex = Number(regexResult[4] ?? arrayLength)
  const stepSize = Number(regexResult[5] ?? 1)

  return {
    startIndex: convertIfNegative(startIndex, arrayLength),
    endIndex: convertIfNegative(endIndex, arrayLength),
    stepSize,
  }
}

function convertIfNegative(index: number, arrayLength: number) {
  return index < 0 ?
    convertNegativeIndex(index, arrayLength, true)
    : index
}

export {}