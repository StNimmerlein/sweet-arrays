import '../src/arrayFunctions'

describe('arrayFunctions', () => {
  describe('sum', () => {
    it('is 0 for empty array', () => {
      expect([].sum()).toBe(0)
    })

    it('sums up values in array', () => {
      expect([1, 2, 3, 15, 2].sum()).toBe(23)
      expect([1, -2, 3, 15, 2].sum()).toBe(19)
    })

    it('can only be invoked for number arrays', () => {
      // @ts-expect-error
      ['a', 'b'].sum();
      // @ts-expect-error
      [true, false].sum()
    })
  })

  describe('prod', () => {
    it('is 1 for empty array', () => {
      expect([].prod()).toBe(1)
    })

    it('multiplies up values in array', () => {
      expect([1, 2, 3, 15, 2].prod()).toBe(180)
      expect([1, -2, 3, 15, 2].prod()).toBe(-180)
      expect([1, 2, 0, 15, 2].prod()).toBe(0)
      expect([1, -2, 0, 15, 2].prod()).toBe(-0)
    })

    it('can only be invoked for number arrays', () => {
      // @ts-expect-error
      ['a', 'b'].prod();
      // @ts-expect-error
      [true, false].prod()
    })
  })

  describe('max', () => {
    it('finds max value', () => {
      expect([1, 4, -4, -6, 2].max()).toBe(4)
      expect([-1, -2, -3].max()).toBe(-1)
    })

    it('throws error for emtpy array', () => {
      expect(() => [].max()).toThrowError()
      expect(() => [].max(() => -1)).toThrowError()
    })

    it('returns value for single value array', () => {
      expect([1].max()).toBe(1)
      expect([-1].max()).toBe(-1)
    })

    it('requires comparator for other types than number', () => {
      // @ts-expect-error
      ['1', '2'].max();
      // @ts-expect-error
      [true, false].max()

      expect(['1', '2'].max((a, b) => Number(a) - Number(b))).toBe('2')
    })
  })

  describe('min', () => {
    it('finds min value', () => {
      expect([1, 4, -4, -6, 2].min()).toBe(-6)
      expect([-1, -2, -3].min()).toBe(-3)
      expect([1, 2, 3].min()).toBe(1)
    })

    it('throws error for emtpy array', () => {
      expect(() => [].min()).toThrowError()
      expect(() => [].min(() => -1)).toThrowError()
    })

    it('returns value for single value array', () => {
      expect([1].min()).toBe(1)
      expect([-1].min()).toBe(-1)
    })

    it('requires comparator for other types than number', () => {
      // @ts-expect-error
      ['1', '2'].min();
      // @ts-expect-error
      [true, false].min()

      expect(['1', '2'].min((a, b) => Number(a) - Number(b))).toBe('1')

    })
  })

  describe('buffer', () => {
    it('groups array', () => {
      expect([1, 2, 3, 4, 5, 6, 7, 8].buffer(2)).toEqual([[1, 2], [3, 4], [5, 6], [7, 8]])
    })

    it('keeps module in last entry', () => {
      expect([1, 2, 3, 4, 5, 6, 7, 8].buffer(3)).toEqual([[1, 2, 3], [4, 5, 6], [7, 8]])
    })

    it('is empty for empty array', () => {
      expect([].buffer(3)).toEqual([])
    })

    it('is empty for zero or negative count', () => {
      expect([1, 2, 3].buffer(0)).toEqual([])
      expect([1, 2, 3].buffer(-1)).toEqual([])
    })

    it('can start every nth character', () => {
      expect([1, 2, 3, 4, 5, 6, 7, 8].buffer(3, 3)).toEqual([[1, 2, 3], [4, 5, 6], [7, 8]]) // 0 3 6
      expect([1, 2, 3, 4, 5, 6, 7, 8].buffer(3, 2)).toEqual([[1, 2, 3], [3, 4, 5], [5, 6, 7], [7, 8]]) // 0 2 4 6 7
      expect([1, 2, 3, 4, 5, 6, 7, 8].buffer(3, 1)).toEqual([[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 7], [6, 7, 8], [7, 8], [8]]) // 0 1
    })
  })

  describe('split', () => {
    it('splits on entries that satisfy the predicate', () => {
      expect([1, 4, -2, 5, -1, -4, 5, 1, -4].split(a => a < 0)).toEqual(
        [[1, 4], [5], [], [5, 1], []],
      )
      expect([1, 4, -2, 5, -1, -4, 5, 1, -4].split(a => a < 0, true)).toEqual(
        [[1, 4], [-2, 5], [-1], [-4, 5, 1], [-4]],
      )
    })

    it('is empty on empty array', () => {
      expect([].split(() => false)).toEqual([])
      expect([].split(() => true)).toEqual([])
    })
  })

  describe('intersect', () => {
    it('returns empty array of no intersecting values', () => {
      expect([1, 2, 3].intersect([4, 5, 6], (a, b) => a === b)).toEqual([])
    })

    it('keeps intersecting values in their minimum quantity', () => {
      expect([1, 2, 3, 4].intersect([2, 4, 6, 8], (a, b) => a === b)).toEqual([2, 4])
      expect([1, 2, 3, 4, 2].intersect([2, 4, 6, 8], (a, b) => a === b)).toEqual([2, 4])
      expect([1, 2, 3, 4, 2].intersect([2, 2, 4, 6, 8], (a, b) => a === b)).toEqual([2, 4, 2])
      expect([1, 2, 3, 4, 2].intersect([2, 2, 4, 4, 6, 8], (a, b) => a === b)).toEqual([2, 4, 2])
      expect([1, 2, 3, 4, 2, 4].intersect([2, 2, 4, 4, 6, 8], (a, b) => a === b)).toEqual([2, 4, 2, 4])
    })

    it('is empty when intersecting with empty array', () => {
      expect([1, 2, 3].intersect([], (a, b) => a === b)).toEqual([])
    })

    it('is empty on empty array', () => {
      expect([].intersect([1, 2, 3], (a, b) => a === b)).toEqual([])
    })

    it('does not need equalFn for same primitive types', () => {
      expect([1, 2, 3, 4].intersect([2, 4, 6, 8])).toEqual([2, 4])
      expect(['never', 'gonna', 'give', 'you', 'up'].intersect(['never', 'gonna', 'let', 'you', 'down'])).toEqual(['never', 'gonna', 'you'])
      expect([true, true, false].intersect([false])).toEqual([false])
    })

    it('requires equalFn for different or non-primitive types', () => {
      // @ts-expect-error
      [{ id: 1 }, { id: 3 }, { id: 2 }].intersect([{ id: 3 }])

      expect([{ id: 1 }, { id: 3 }, { id: 2 }].intersect([{ id: 3 }], (a, b) => a.id === b.id)).toEqual([{ id: 3 }]);

      // @ts-expect-error
      [1, 2, 3].intersect(['1', '3'])

      expect([1, 2, 3].intersect(['1', '3'], (a, b) => a === Number(b))).toEqual([1, 3])
    })
  })

  describe('remove', () => {
    it('removes elements with index', () => {
      expect([1, 2, 3, 4].remove(1)).toEqual([1, 3, 4])
    })

    it('removes elements with negative index from behind', () => {
      expect([1, 2, 3, 4].remove(-1)).toEqual([1, 2, 3])
      expect([1, 2, 3, 4].remove(-4)).toEqual([2, 3, 4])
    })

    it('throws error when index out of scope', () => {
      expect(() => [1, 2, 3, 4].remove(4)).toThrowError()
      expect(() => [1, 2, 3, 4].remove(-5)).toThrowError()
    })

  })

  describe('removeFirst', () => {
    it('removes first element that satisfies predicate', () => {
      expect([1, 2, 3, 4, 5].removeFirst(a => a >= 4)).toEqual([1, 2, 3, 5])
      expect([1, 2, 3, 4, 5].removeFirst(a => a >= 6)).toEqual([1, 2, 3, 4, 5])
    })
  })

  describe('unique', () => {
    it('is empty on empty array', () => {
      expect([].unique()).toEqual([])
    })

    it('uses identity comparison if no comparator given', () => {
      expect([1, 2, 4, 1, 5, 2].unique()).toEqual([1, 2, 4, 5])
      expect(['never', 'gonna', 'give', 'you', 'up', 'never', 'gonna', 'let', 'you', 'down'].unique())
        .toEqual(['never', 'gonna', 'give', 'you', 'up', 'let', 'down'])
      expect([{ id: 1 }, { id: 2 }, { id: 1 }].unique()).toEqual([{ id: 1 }, { id: 2 }, { id: 1 }])
    })

    it('uses equalFn to detect equality', () => {
      expect([1, 2, 4, 1, 3].unique(() => true)).toEqual([1])
      expect([{ id: 1 }, { id: 2 }, { id: 1 }].unique((a, b) => a.id === b.id)).toEqual([{ id: 1 }, { id: 2 }])
    })
  })

  describe('first', () => {
    it('gets first element', () => {
      expect([1, 2, 3].first()).toBe(1)
    })

    it('throws error on empty array', () => {
      expect(() => [].first()).toThrowError()
    })
  })

  describe('last', () => {
    it('gets last element', () => {
      expect([1, 2, 3].last()).toBe(3)
    })

    it('throws error on empty array', () => {
      expect(() => [].last()).toThrowError()
    })
  })

  describe('count', () => {
    it('returns 0 for empty array', () => {
      expect([].count(() => true)).toBe(0)
    })

    it('counts the elements that satisfy the predicate', () => {
      expect([1, 2, 3, 4, 5, 6].count(a => a % 2 === 0)).toBe(3)
    })
  })

  describe('transpose', () => {
    it('is empty for empty array', () => {
      expect([].transpose()).toEqual([])
    })

    it('transposes vector', () => {
      expect([[1, 2, 3, 4]].transpose()).toEqual([[1], [2], [3], [4]])
      expect([[1], [2], [3], [4]].transpose()).toEqual([[1, 2, 3, 4]])
    })

    it('transposes matrix', () => {
      expect([
        ['a', 'b', 'c'],
        ['d', 'e', 'f'],
        ['g', 'h', 'i'],
        ['j', 'k', 'm'],
      ].transpose()).toEqual([
        ['a', 'd', 'g', 'j'],
        ['b', 'e', 'h', 'k'],
        ['c', 'f', 'i', 'm'],
      ])
    })

    it('fills shorter lines with undefined by default', () => {
      expect([
        ['a', 'b', 'c'],
        ['d', 'e'],
        ['f', 'g', 'h', 'i', 'j'],
      ].transpose()).toEqual([
        ['a', 'd', 'f'],
        ['b', 'e', 'g'],
        ['c', undefined, 'h'],
        [undefined, undefined, 'i'],
        [undefined, undefined, 'j'],
      ])
    })

    it('fills can ignore unfilled lines', () => {
      expect([
        ['a', 'b', 'c'],
        ['d', 'e'],
        ['f', 'g', 'h', 'i', 'j'],
      ].transpose(false)).toEqual([
        ['a', 'd', 'f'],
        ['b', 'e', 'g'],
        ['c', 'h'],
        ['i'],
        ['j'],
      ])
    })
  })

  describe('get', () => {
    it('is empty for empty array', () => {
      expect([].get(':')).toEqual([])
    })

    describe(':', () => {
      it('returns copy of whole array', () => {
        expect([1, 2, 3, 4].get(':')).toEqual([1, 2, 3, 4])
        const array = [1, 2, 3, 4]
        expect(array.get(':')).not.toBe(array)
      })

      it('can include step size', () => {
        expect([1, 2, 3, 4, 5, 6, 7].get('::2')).toEqual([1, 3, 5, 7])
        expect([1, 2, 3, 4, 5, 6, 7].get('::3')).toEqual([1, 4, 7])
        expect([1, 2, 3, 4, 5, 6, 7].get('::1')).toEqual([1, 2, 3, 4, 5, 6, 7])
      })
    })

    describe('${number}:', () => {
      it('returns array from index to end', () => {
        expect([1, 2, 3, 4, 5].get('2:')).toEqual([3, 4, 5])
        expect([1, 2, 3, 4, 5].get('5:')).toEqual([])
      })

      it('can deal with negative numbers', () => {
        expect([1, 2, 3, 4, 5].get('-1:')).toEqual([5])
        expect([1, 2, 3, 4, 5].get('-3:')).toEqual([3, 4, 5])
        expect([1, 2, 3, 4, 5].get('-10:')).toEqual([1, 2, 3, 4, 5])
      })

      it('can include step size', () => {
        expect([1, 2, 3, 4, 5, 6, 7].get('2::2')).toEqual([3, 5, 7])
        expect([1, 2, 3, 4, 5, 6, 7].get('-4::2')).toEqual([4, 6])
      })
    })

    describe(':${number}', () => {
      it('returns array from start to index (excluding)', () => {
        expect([1, 2, 3, 4, 5].get(':3')).toEqual([1, 2, 3])
        expect([1, 2, 3, 4, 5].get(':5')).toEqual([1, 2, 3, 4, 5])
        expect([1, 2, 3, 4, 5].get(':8')).toEqual([1, 2, 3, 4, 5])
      })

      it('can deal with negative numbers', () => {
        expect([1, 2, 3, 4, 5].get(':-1')).toEqual([1, 2, 3, 4])
        expect([1, 2, 3, 4, 5].get(':-3')).toEqual([1, 2])
        expect([1, 2, 3, 4, 5].get(':-10')).toEqual([])
      })

      it('can include step size', () => {
        expect([1, 2, 3, 4, 5, 6, 7].get(':5:2')).toEqual([1, 3, 5])
        expect([1, 2, 3, 4, 5, 6, 7].get(':-2:2')).toEqual([1, 3, 5])
      })
    })

    describe('${number}:${number}', () => {
      it('returns array from startIndex to endIndex (excluding)', () => {
        expect([1, 2, 3, 4, 5].get('1:3')).toEqual([2, 3])
        expect([1, 2, 3, 4, 5].get('3:3')).toEqual([])
        expect([1, 2, 3, 4, 5].get('0:5')).toEqual([1, 2, 3, 4, 5])
        expect([1, 2, 3, 4, 5].get('-3:-1')).toEqual([3, 4])
        expect([1, 2, 3, 4, 5].get('-3:-4')).toEqual([])
        expect([1, 2, 3, 4, 5].get('-3:4')).toEqual([3, 4])
        expect([1, 2, 3, 4, 5].get('1:3:2')).toEqual([2])
        expect([1, 2, 3, 4, 5].get('3:3:2')).toEqual([])
        expect([1, 2, 3, 4, 5].get('0:5:2')).toEqual([1, 3, 5])
        expect([1, 2, 3, 4, 5].get('-3:-1:2')).toEqual([3])
        expect([1, 2, 3, 4, 5].get('-3:-4:2')).toEqual([])
        expect([1, 2, 3, 4, 5].get('-3:4:2')).toEqual([3])
      })
    })
  })

  describe('scan', () => {
    it('returns intermediate results', () => {
      expect([1, 2, 3, 4, 5, 6, 7].scan((a, b) => a + b)).toEqual(
        [1, 3, 6, 10, 15, 21, 28],
      )

      expect([1, 2, 3, 4, 5, 6, 7].scan((a, b) => a + b, 0)).toEqual(
        [1, 3, 6, 10, 15, 21, 28],
      )

      expect([1, 2, 3, 4, 5].scan((a, b) => a + b, '')).toEqual(
        ['1', '12', '123', '1234', '12345'],
      )

      expect(['one', 'two', 'three'].scan((a, b, index) => `${ a }\n${ index }: ${ b }`, '')).toEqual(
        ['\n0: one', '\n0: one\n1: two', '\n0: one\n1: two\n2: three'],
      )
    })

    it('is empty on  empty array', () => {
      expect(([] as number[]).scan((a, b) => a + b)).toEqual([])

      expect(([] as number[]).scan((a, b) => a + b, 0)).toEqual([])
    })
  })

  describe('startWith', () => {
    it('appends value to front', () => {
      expect([1, 2, 3].startWith(0)).toEqual([0, 1, 2, 3])
      expect(([] as string[]).startWith('Hallo')).toEqual(['Hallo'])
    })
  })

  describe('sideEffect', () => {
    it('executes the function and returns the unchanged array', () => {
      const mockFn = jest.fn()
      const array = [1, 2, 'Test']

      const result = array.sideEffect(mockFn)

      expect(mockFn).toHaveBeenCalledWith(1)
      expect(mockFn).toHaveBeenCalledWith(2)
      expect(mockFn).toHaveBeenCalledWith('Test')
      expect(result).toEqual([1, 2, 'Test'])
    })
  })

  describe('sort', () => {
    describe('array only contains numbers', () => {
      describe('no function given', () => {
        it('sorts by number comparison', () => {
          expect([1, 5, 2, 7, 8, 4].sort()).toEqual([1, 2, 4, 5, 7, 8])
        })
      })

      describe('comparison function given', () => {
        it('sorts by comparison function', () => {
          const order = [7, 3, 4, 1, 6, 2, 5]
          const compareFn = (a: number, b: number) => order.indexOf(a) - order.indexOf(b)

          expect([1, 2, 3, 4].sort(compareFn)).toEqual([3, 4, 1, 2])
        })
      })

      describe('mapper function given', () => {
        it('sorts mapped elements using number sort', () => {
          const order = [7, 3, 4, 1, 6, 2, 5]
          const mapperFn = (a: number) => order.indexOf(a)

          expect([1, 2, 3, 4].sort(mapperFn)).toEqual([3, 4, 1, 2])
        })
      })
    })

    describe('array contains at least one non-number', () => {
      describe('no function given', () => {
        it('sorts by the default sort function', () => {
          expect(['a', 'ax', 'bb', 'ac', 'z', 'b'].sort()).toEqual(['a', 'ac', 'ax', 'b', 'bb', 'z'])
          expect([1, 4, 2, 6, 12, 'b'].sort()).toEqual([1, 12, 2, 4, 6, 'b'])
        })
      })

      describe('comparison function given', () => {
        it('sorts by comparison function', () => {
          const compareFn = (a: string, b: string) => a.length - b.length

          expect(['one', 'xs', 'longer', 'someLongString', 'mediumString'].sort(compareFn)).toEqual(['xs', 'one', 'longer', 'mediumString', 'someLongString'])
        })
      })

      describe('mapper function given', () => {
        it('sorts mapped elements using number sort', () => {
          const mapperFn = (a: string) => a.length

          expect(['one', 'xs', 'longer', 'someLongString', 'mediumString'].sort(mapperFn)).toEqual(['xs', 'one', 'longer', 'mediumString', 'someLongString'])
        })
      })
    })
  })

  describe('groupSameElements', () => {
    it('uses provided equalsFn', () => {
      expect([1, 2, 4, 6, 4, 5, 7, 4, 6, 3, 10].groupSameElements((a, b) => a % 2 === b % 2)).toEqual([[1], [2, 4, 6, 4], [5, 7], [4, 6], [3], [10]])
    })

    it('uses default equality if no equalsFn provided', () => {
      expect([1, 2, 3, 3, 6, 3, 5, 5, 5, 5, 5, 4, 4, 3].groupSameElements()).toEqual([[1], [2], [3, 3], [6], [3], [5, 5, 5, 5, 5], [4, 4], [3]])
    })
  })

  describe('getAllPermutations', () => {
    it('returns empty array for empty array', () => {
      expect([].getAllPermutations()).toEqual([])
    })

    it('returns single permutations for single element array', () => {
      expect([1].getAllPermutations()).toEqual([[1]])
    })

    it('does not check entries in source array for duplicates', () => {
      expect([1, 1].getAllPermutations()).toEqual([[1, 1], [1, 1]])
    })

    it('returns all permutations', () => {
      const permutations = [1, 2, 3, 4].getAllPermutations()
      expect(permutations).toHaveLength(24)
      expect(permutations).toContainEqual([1, 2, 3, 4])
      expect(permutations).toContainEqual([1, 2, 4, 3])
      expect(permutations).toContainEqual([1, 3, 2, 4])
      expect(permutations).toContainEqual([1, 3, 4, 2])
      expect(permutations).toContainEqual([1, 4, 2, 3])
      expect(permutations).toContainEqual([1, 4, 3, 2])
      expect(permutations).toContainEqual([2, 3, 4, 1])
      expect(permutations).toContainEqual([2, 3, 1, 4])
      expect(permutations).toContainEqual([2, 4, 3, 1])
      expect(permutations).toContainEqual([2, 4, 1, 3])
      expect(permutations).toContainEqual([2, 1, 3, 4])
      expect(permutations).toContainEqual([2, 1, 4, 3])
      expect(permutations).toContainEqual([3, 1, 2, 4])
      expect(permutations).toContainEqual([3, 1, 4, 2])
      expect(permutations).toContainEqual([3, 2, 1, 4])
      expect(permutations).toContainEqual([3, 2, 4, 1])
      expect(permutations).toContainEqual([3, 4, 1, 2])
      expect(permutations).toContainEqual([3, 4, 2, 1])
      expect(permutations).toContainEqual([4, 1, 2, 3])
      expect(permutations).toContainEqual([4, 1, 3, 2])
      expect(permutations).toContainEqual([4, 2, 1, 3])
      expect(permutations).toContainEqual([4, 2, 3, 1])
      expect(permutations).toContainEqual([4, 3, 1, 2])
      expect(permutations).toContainEqual([4, 3, 2, 1])
    })
  })

  describe('partition', () => {
    it('does nothing on empty array', () => {
      expect([].partition(() => true)).toEqual([[], []])
    })

    it('can deal with all elements satisfying the predicate', () => {
      expect([1, 2, 3].partition(() => true)).toEqual([[1, 2, 3], []])
    })

    it('can deal with no elements satisfying the predicate', () => {
      expect([1, 2, 3].partition(() => false)).toEqual([[], [1, 2, 3]])
    })

    it('partitions the array and keeps the order intact', () => {
      expect([1, 5, 3, 7, 5, 6, 8, 8, 6, 3, 5, 2, 7, 2, 3, 1].partition(e => e > 4)).toEqual([
          [5, 7, 5, 6, 8, 8, 6, 5, 7],
          [1, 3, 3, 2, 2, 3, 1]
      ])
    })
  })
})