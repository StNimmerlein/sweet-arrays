# sweet-arrays

This library provides nice utility functions for array manipulation in JavaScript and TypeScript. All functions are implemented as extension functions for the `Array` interface.

All functions do **not** change the source array and return a new array instead. If no modifications are made, the original array may be returned. 

## Install

Add the library to your dependencies (`npm i sweet-arrays` or `yarn add sweet-arrays`). Then:

## Usage
- For commonjs JavaScript projects: `require('sweet-arrays')`
- For commonjs TypeScript or module JavaScript projects: `import 'sweet-arrays'`

That's it, now you can use all methods below.

## Methods

For more examples per function look at the tests in the main repository.

### Overview

The following functions are currently provided. For more details see below.
- get: Select a partial array
- first: Select the first element
- last: Select the last element
- remove: Remove item at given index
- removeFirst: Remove first item that satisfies predicate
- unique: Remove duplicate items
- sum: Sum up all items
- max: Find the maximal element
- min: Find the minimal element
- count: Count all elements that satisfy a predicate
- scan: Like `reduce` but get an array with all intermediate results
- startWith: Append given value to beginning of array
- buffer: Group items in batches of same size
- split: Like split for strings
- intersect: Get all values from the intersection of two arrays
- transpose: Interpret a 2D-array as matrix and transpose it
- sideEffect: Call a given function for each element and return the unchanged array (similar to `forEach` but without ending the chain)
- sort: Extend default `sort` method of arrays to use number comparison if the whole array consists of numbers
- groupSameElements: Splits the array whenever the value changes
- getAllPermutations: Get all permutations of an array
- partition: Splits the array into two partitions: One with all elements satisfying a given predicate, the other with the rest


### get
Select a partial array by giving a Python-like array selector string.

You can specify the first index (included) of the subarray, the last index (excluded) and a step size. Each value can be omitted if you want to use the default (start index `0`, end index `array.length`, step size `1`).

Negative indices are counted from the back.

```typescript
const sourceArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
sourceArray.get('1:-1') // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
sourceArray.get('1:-1:3') // [ 1, 4, 7, 10, 13 ]
sourceArray.get(':-3') // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
sourceArray.get(':13') // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
sourceArray.get('-3:') // [13, 14, 15]
```

### first
Returns the first element of the array.
```typescript
[3, 5, 2].first() // 3
```

### last
Returns the last element of the array.
```typescript
[3, 5, 2].last() // 2
```

### remove
Removes the element at given index. Note: This does not modify the source array but returns a new array without this element.
```typescript
[0, 1, 2, 3, 4].remove(2) // [0, 1, 3, 4]
[0, 1, 2, 3, 4].remove(-2) // [0, 1, 2, 4]
```

### removeFirst
Removes the first element that fulfills the given predicate.
```typescript
[0, 1, 2, 3, 4, 5].removeFirst(a => a > 2) // [0, 1, 2, 4, 5]
```

### unique
Removes duplicates in the array and only keeps the first appearance of each item. If no equal function is provided, it will use `===` to check for equality.
```typescript
[0, 4, 2, 5, 6, 2, 4, 4, 7].unique() // [0, 4, 2, 5, 6, 7]
[{ value: 0 }, { value: 4 }, { value: 2 }, { value: 5 }, { value: 6 }, { value: 2 }, { value: 4 }, { value: 4 }, { value: 7 }].unique() // [{ value: 0 }, { value: 4 }, { value: 2 }, { value: 5 }, { value: 6 }, { value: 2 }, { value: 4 }, { value: 4 }, { value: 7 }]
[{ value: 0 }, { value: 4 }, { value: 2 }, { value: 5 }, { value: 6 }, { value: 2 }, { value: 4 }, { value: 4 }, { value: 7 }].unique((a, b) => a.value === b.value) // [{ value: 0 }, { value: 4 }, { value: 2 }, { value: 5 }, { value: 6 }, { value: 7 }]
```

### sum
Only available on number arrays. Sums up the array entries.
```typescript
[1, 4, 5, 2].sum() // 12
```

### max
On non-number arrays a comparator function needs to be provided. Finds the maximal array entry.
```typescript
[1, 4, 5, 2].max() // 5
['1', '4', '5', '2'].max((a, b) => Number(a) - Number(b)) // '5'
```

### min
On non-number arrays a comparator function needs to be provided. Finds the minimal array entry.
```typescript
[1, 4, 5, 2].min() // 1
['1', '4', '5', '2'].min((a, b) => Number(a) - Number(b)) // '1'
```

### count
Counts the elements that satisfy the given predicate.
```typescript
[1, 4, 2, 5, 6, 2, 4, 6].count(a => a > 3) // 5
```

### scan
Similar to reduce but instead of only returning the final result it returns an array with all the intermediate results. The reducer function can take the current index as optional third parameter.
```typescript
[1, 2, 3, 4, 5, 6].scan((akk, val) => akk + val) // [1, 3, 6, 10, 15, 21]
[1, 2, 3, 4, 5, 6].scan((akk, val) => akk + val, 10) // [11, 13, 16, 20, 25, 31]
```

### startWith
Adds the given value to the beginning of the array.
```typescript
[1, 2, 3, 4].startWith(10) // [10, 1, 2, 3, 4]
```

### buffer
Groups the array entries in groups of a given size n. Optionally you can specify when a new buffer should start; default is every n-th element.
```typescript
[0, 1, 2, 3, 4, 5, 6, 7].buffer(3) // [[0, 1, 2], [3, 4, 5], [6, 7]]
[0, 1, 2, 3, 4, 5, 6, 7].buffer(3, 1) // [[0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 7], [6, 7], [7]]
[0, 1, 2, 3, 4, 5, 6, 7].buffer(3, 2) // [[0, 1, 2], [2, 3, 4], [4, 5, 6], [6, 7]]
```

### split
Similar to String.split: splits the array at these entries that satisfy the predicate. You can specify whether to keep the entries at the cutting points or not (default ist `false`).
```typescript
[1, 3, 6, 3, 7, 6, 2, 1, 1].split(e => e > 5) // [[1, 3], [3], [], [2, 1, 1]]
[1, 3, 6, 3, 7, 6, 2, 1, 1].split(e => e > 5, true) // [[1, 3], [6, 3], [7], [6, 2, 1, 1]]
```

### intersect
Returns the elements that appear in both arrays (in their minimal appearance). For arrays of other types than number, string or boolean, an equals function needs to be provided.
The entries in the result appear in the same order as in the source array.
```typescript
[0, 1, 2, 3, 4, 5].intersect([0, 2, 4, 6, 8]) // [0, 2, 4]
[1, 5, 2, 7, 3, 5, 2].intersect([5, 7, 2, 4, 6, 5, 5, 6]) // [5, 2, 7, 5]
[{ value: 1 }, { value: 5 }, { value: 2 }, { value: 7 }, { value: 3 }, { value: 5 }, { value: 2 }].intersect(
  [{ value: 5 }, { value: 7 }, { value: 2 }, { value: 4 }, { value: 6 }, { value: 5 }, { value: 5 }, { value: 6 }],
  (a, b) => a.value === b.value
) // [{ value: 5 }, { value: 2 }, { value: 7 }, { value: 5 }]
```

### transpose
Only applicable to two-dimensional arrays. Interprets the array as a matrix and transposes it. You can specify if rows should be filled with `undefined` if they are shorter than the longest row (default is `true`).
```typescript
[
  ['a', 'b', 'c', 'd'],
  ['e', 'f', 'g', 'h'],
  ['i', 'j', 'k', 'l']
].transpose()
/*
[
  ['a', 'e', 'i'],
  ['b', 'f', 'j'],
  ['c', 'g', 'k'],
  ['d', 'h', 'l']
]
 */
[
  ['a', 'b', 'c'],
  ['e', 'f', 'g', 'h'],
  ['i', 'j']
].transpose()
/*
[
  ['a', 'e', 'i'],
  ['b', 'f', 'j'],
  ['c', 'g', undefined],
  [undefined, 'h', undefined]
]
 */
  [
  ['a', 'b', 'c'],
    ['e', 'f', 'g', 'h'],
    ['i', 'j']
  ].transpose(false)
/*
[
  ['a', 'e', 'i'],
  ['b', 'f', 'j'],
  ['c', 'g'],
  ['h']
]
 */
```

### sideEffect
Run side effect for each element of the array. Similar to `forEach` but does not end the chain.
```typescript
[1, 3, 6, 3, 7, 6, 2, 1, 1].sideEffect(e => console.log(e)).map(someMapFn)...
```